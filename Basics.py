def foo(input):
    return input*3;

myname = "isuru udukala";
myage = 23;
temp = "VARDATA";
temp2= "VARDATA2";

print(f"f-string test - {myname}");
print("%%-formatting test - %s" % myname);
print("str.format() test - ({1}) , ({0})".format(temp, temp2));

print(f"function test. input = 10 - {foo(10)}");
print(f"function test. input = \"Test\" - {foo('Test')}");

triplequote = '''line1
line2
line3''';

inputvar = input("input: ");
print(inputvar);
